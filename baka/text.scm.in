;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (baka text)
  #:use-module (baka markup)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (rnrs bytevectors)
  #:use-module (sdl2)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (system foreign)
  #:export (%text-pixel-format

            make-text-field
            text-content
            text-attributes text-markup
            text-font
            text-alignment
            text-num-revealed
            text-fill-rgba text-stroke-rgba
            text-stroke-width
            text-spacing set-text-spacing!
            paint!

            add-font-dir!))

(eval-when (compile load eval)
  (load-extension
   (if (getenv "LIBBAKA_UNINSTALLED")
       "libbaka.so"
       "@tsukundere_extensiondir@/libbaka.so")
   "baka_init_scm_text"))

(define %double4 (list double double double double))
(define %paint-options-structure (list int int %double4 %double4 double))
(define %paint-options-offsets
  (list->vector
   (reverse
    (fold
     (lambda (cur seed)
       (cons
        (+ (sizeof cur) (car seed))
        seed))
     (list 0)
     %paint-options-structure))))

(define %text-pixel-format
  (case (native-endianness)
    ((little) 'argb8888)
    ((big)    'bgra8888)))

(define unwrap-renderer (@@ (sdl2 render) unwrap-renderer))
(define wrap-texture (@@ (sdl2 render) wrap-texture))
(define unwrap-texture (@@ (sdl2 render) unwrap-texture))

(define-record-type <text-field>
  (%make-text-field layout %paint-options %n-revealed %valign
                    fill-rgba stroke-rgba %stroke-width)
  text-field?
  (layout         text-layout)
  (%paint-options %text-paint-options)
  (%n-revealed    %text-n-revealed)
  (%valign        %text-valign)
  (fill-rgba      text-fill-rgba)
  (stroke-rgba    text-stroke-rgba)
  (%stroke-width  %text-stroke-width))

(define %valign->number '((top . 0) (center . 1) (bottom . 2)))
(define %valign<-number
  (map (lambda (pair) (cons (cdr pair) (car pair))) %valign->number))

;; Allocate a struct holding
;; @itemize
;; @item n-revealed
;; The number of revealed characters.
;; @item fill-rgba
;; The color with which to fill the text.
;; You do not need to divide this SDL color by 255 --
;; @code{make-paint-options} already does that for you.
;; @item stroke-rgba
;; The color with which to stroke the border of the text.
;; You do not need to divide this SDL color by 255 --
;; @code{make-paint-options} already does that for you.
;; @item stroke-width
;; The width of said border.
;; @end itemize
(define* (make-paint-options #:key
                             (n-revealed 0)
                             (valign 'top)
                             (fill-rgba (make-color 255 255 255 255))
                             (stroke-rgba (make-color 0 0 0 255))
                             (stroke-width 1.0))
  (make-c-struct %paint-options-structure
                 (list n-revealed
                       (assoc-ref %valign->number valign)
                       (list (/ (color-r fill-rgba) 255)
                             (/ (color-g fill-rgba) 255)
                             (/ (color-b fill-rgba) 255)
                             (/ (color-a fill-rgba) 255))
                       (list (/ (color-r stroke-rgba) 255)
                             (/ (color-g stroke-rgba) 255)
                             (/ (color-b stroke-rgba) 255)
                             (/ (color-a stroke-rgba) 255))
                       stroke-width)))

;; Allocate a new Pango layout, pass @var{args} to @code{make-paint-options}
;; (which see) and combine both into a single text field.
(define (make-text-field . args)
  (let ((paint-options (apply make-paint-options args)))
    (%make-text-field
     (%make-layout)
     paint-options
     (pointer->bytevector paint-options 1
                          (vector-ref %paint-options-offsets 0)
                          's32)
     (pointer->bytevector paint-options 1
                          (vector-ref %paint-options-offsets 1)
                          's32)
     (pointer->bytevector paint-options 4
                          (vector-ref %paint-options-offsets 2)
                          'f64)
     (pointer->bytevector paint-options 4
                          (vector-ref %paint-options-offsets 3)
                          'f64)
     (pointer->bytevector paint-options 1
                          (vector-ref %paint-options-offsets 4)
                          'f64))))

(define text-content
  (make-procedure-with-setter
   (lambda (text)
     (%layout-get-text (text-layout text)))
   (lambda (text value)
     (%layout-set-text! (text-layout text) value))))

(define text-attributes
  (make-procedure-with-setter
   (lambda (text)
     (%layout-get-attributes (text-layout text)))
   (lambda (text value)
     (%layout-set-attributes! (text-layout text) value))))

(define text-markup
  (make-procedure-with-setter
   (lambda (text)
     (error "markup can not be read; only written"))
   (lambda (text markup)
     (let ((layout (text-layout text))
           (content attributes (parse-markup markup)))
       (%layout-set-text! layout content)
       (%layout-set-attributes! layout attributes)))))

(define text-font
  (make-procedure-with-setter
   (lambda (text)
     (%layout-get-font (text-layout text)))
   (lambda (text value)
     (%layout-set-font! (text-layout text) value))))

(define text-alignment
  (make-procedure-with-setter
   (lambda (text)
     (cons
      (%layout-get-alignment (text-layout text))
      (assoc-ref %valign<-number
                 (bytevector-s32-native-ref (%text-valign text) 0))))
   (match-lambda*
     ((text ((? (cute member <> '(left center right)) halign)
             .
             (? (cute member <> '(top center bottom)) valign)))
      (%layout-set-alignment! (text-layout text) halign)
      (bytevector-s32-native-set! (%text-valign text) 0
                                  (assoc-ref %valign->number valign))))))

(define text-num-revealed
  (make-procedure-with-setter
   (lambda (text)
     (bytevector-s32-native-ref (%text-n-revealed text) 0))
   (lambda (text value)
     (bytevector-s32-native-set! (%text-n-revealed text) 0 value))))

(define text-stroke-width
  (make-procedure-with-setter
   (lambda (text)
     (bytevector-ieee-double-native-ref (%text-stroke-width text) 0))
   (lambda (text value)
     (bytevector-ieee-double-native-set! (%text-stroke-width text) 0 value))))

;; Paint @var{text} onto @var{texture}.
(define (paint! texture text)
  (%paint! (unwrap-texture texture)
           (text-layout text)
           (%text-paint-options text)))

(define* (set-text-spacing! text line-spacing #:optional spacing)
  (%layout-set-spacing! (text-layout text) line-spacing spacing))

(define text-spacing
  (make-procedure-with-setter
   (lambda (text)
     (%layout-get-spacing (text-layout text)))
   (lambda (text spacing*)
     (apply set-text-spacing! text spacing*))))
