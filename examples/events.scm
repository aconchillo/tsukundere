;; Copyright © 2020 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; Copying and distribution of this file, with or without modification,
;; are permitted in any medium without royalty provided the copyright
;; notice and this notice are preserved.  This file is offered as-is,
;; without any warranty.

(use-modules (tsukundere)
             (srfi srfi-26))

(define (init)
  (init-script! load-script))

(define (peek . stuff)
  (display ";; ")
  (write stuff)
  (newline)
  (car (last-pair stuff)))

(define (load-script)
  (call/paused
   (lambda () (pause))
   #:key-press     (cute peek 'key-press <...>)
   #:key-release   (cute peek 'key-release <...>)
   #:mouse-press   (cute peek 'mouse-press <...>)
   #:mouse-release (cute peek 'mouse-release <...>)
   #:mouse-move    (cute peek 'mouse-move <...>)
   #:text-input    (cute peek 'text-input <...>)))

(run-game #:init! init #:game-name #f)
