(use-modules (guix gexp)
             (guix packages)
             (guix git-download)
             (guix utils)
             (guix build-system gnu)
             (gnu packages autotools)
             ((gnu packages game-development) #:prefix guix:)
             (gnu packages gettext)
             (gnu packages gtk)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages pkg-config)
             (gnu packages sdl)
             (gnu packages texinfo)
             ((guix licenses) #:select (lgpl3+)))

(define %source-dir (dirname (current-filename)))

;; If this recipe breaks, time-machine to fed33f701367cae49cfa1a4c619dade3d735b575
(define tsukundere
  (package
    (inherit guix:tsukundere)
    (version "0.4.2+git")
    (source (local-file %source-dir
                        #:recursive? #t
                        #:select? (git-predicate %source-dir)))
    (arguments
     `(#:modules ((ice-9 match)
                  (srfi srfi-1)
                  ((guix build guile-build-system)
                   #:select (target-guile-effective-version))
                  ,@%gnu-build-system-modules)
       #:imported-modules ((guix build guile-build-system)
                           ,@%gnu-build-system-modules)
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'patch-command
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((scm (lambda (in)
                           (string-append in "/share/guile/site/"
                                          (target-guile-effective-version))))
                    (ccache (lambda (in)
                              (string-append in "/lib/guile/"
                                             (target-guile-effective-version)
                                             "/site-ccache")))
                    (pkgs
                     (cons
                      (assoc-ref outputs "out")
                      (filter-map
                       (match-lambda
                         (("guile" . pkg) pkg)
                         ((label . pkg)
                          (and (string-prefix? "guile-" label) pkg)))
                       inputs))))
               (substitute* "tsukundere.scm"
                 (("exec guile (.*)" _ args)
                  (string-append
                   (format #f "export GUILE_LOAD_PATH=\"~@?\"~%"
                           "~{~a~^:~}" (map scm pkgs))
                   (format #f "export GUILE_LOAD_COMPILED_PATH=\"~@?\"~%"
                           "~{~a~^:~}" (map ccache pkgs))
                   "exec "
                   (assoc-ref inputs "guile")
                   "/bin/guile " args)))
               #t))))))
    ;; Let's still control the inputs, just to be safe
    (native-inputs
     `(("autoconf" ,autoconf-wrapper)
       ("automake" ,automake)
       ("gettext" ,gettext-minimal)
       ("guile" ,guile-3.0)
       ("libtool" ,libtool)
       ("pkg-config" ,pkg-config)
       ("texinfo" ,texinfo)))
    (inputs
     `(("guile-sdl2" ,guile-sdl2)
       ("guile" ,guile-3.0)
       ("pango" ,pango)
       ("sdl2" ,sdl2)))))

(define tsukundere/wisp
  (package/inherit tsukundere
    (inputs `(("guile-wisp" ,guile-wisp)
              ,@(package-inputs tsukundere)))))

tsukundere
