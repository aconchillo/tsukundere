;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere cli run)
  #:use-module (ice-9 optargs)
  #:use-module (tsukundere assets)
  #:use-module (tsukundere cli)
  #:use-module (tsukundere i18n)
  #:use-module (tsukundere game)
  #:use-module (tsukundere utils)
  #:export (*options* *values* *help*
            run))

(define *options*
  (cons* (option '(#\L "load-path") #t #f
                 (lambda (opt name arg result)
                   (acons 'load-path (cons arg (assoc-ref result 'load-path))
                          result)))
         (option '(#\A "asset-dir") #t #f
                 (lambda (opt name arg result)
                   (if (assoc-ref result 'asset-dir)
                       (fail "`-A' option can not be specified more than once")
                       (acons 'asset-dir arg result))))
         (option '(#\m "module") #t #f
                 (lambda (opt name arg result)
                   (if (assoc-ref result 'module)
                       (fail "`-m' option can not be specified more than once")
                       (acons 'module
                              (map string->symbol (string-split arg #\space))
                              result))))
         (option '("localedir") #t #f
                 (lambda (opt name arg result)
                   (if (assoc-ref result 'locale-dir)
                       (fail "`--localedir' option can not be specified more than once")
                       (acons 'locale-dir arg result))))
         base-options))

(define *values*
  '((load-path)))

(define (*help*)
  (T_ "Usage: tsukundere run [OPTION]...
Run a visual novel using the Tsukundere engine.

  -h, --help            print this help message
  -A, --asset-dir=DIR   set DIR as the directory, from which assets are loaded
  -L, --load-path=DIR   add DIR to the front of the module load path
  -m, --module=MODULE   use MODULE as the entry point (required)
  --localedir=DIR       load translations from DIR"))

(define* (run #:key
              asset-dir
              locale-dir
              help?
              (load-path '())
              module)
  (when help? (show-help "run"))
  (unless module
    (fail "no module given"))

  (when asset-dir (set-asset-path! asset-dir))

  (set! %load-path (append load-path %load-path))
  (set! %load-should-auto-compile #f)

  (apply run-game/module module
         (if locale-dir
             `(#:locale-dir ,locale-dir)
             '())))
