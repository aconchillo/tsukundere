;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere entities)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-26)
  #:use-module (tsukundere components)
  #:export (<entity>
            make-entity entity-name entity-get entity-set! entity-copy!
            entity->procedure))

(define <entity> (make-record-type '<entity> '((immutable name))))

(define %make-entity (record-constructor <entity>))
(define entity-name (record-accessor <entity> 'name))
;; Return a new entity named @var{name}.  If @var{name} is omitted,
;; generate a unique name, that other entities can't use.
(define* (make-entity #:optional name)
  (%make-entity (or name (make-symbol "entity"))))

;; Return the value associated with @var{entity} in
;; the @var{key} component of @var{registry}.
(define-inlinable (entity-get registry entity key)
  (hashq-ref (hashq-ref (unwrap-registry registry) key) entity))

;; Set the value associated with @var{entity} in the @var{key} component of
;; @var{registry} to @var{value}.
(define-inlinable (entity-set! registry entity key value)
  (hashq-set! (hashq-ref (unwrap-registry registry) key) entity value))

;; Return a procedure, that can be used to set components of @var{entity}
;; in @var{registry}.
(define (entity->procedure entity registry)
  (make-procedure-with-setter
   (cute entity-get registry entity <>)
   (cute entity-set! registry entity <> <>)))

;; Copy @var{components} from @var{src} to @var{dst}.
;; Components can either be procedures or pairs of procedures.  If
;; a component is just a procedure, a shallow copy of its value in @var{src}
;; will be made in @var{dst}.  Otherwise, the cdr of the pair will be called
;; to construct a copy of appropriate depth.
(define (entity-copy! dst src components)
  (for-each
   (match-lambda
     ((? procedure? proc)
      (set! (proc dst) (proc src)))
     (((? procedure? proc) . (? procedure? copy))
      (set! (proc dst) (copy (proc src)))))
   components))
