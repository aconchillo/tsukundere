;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere game modules)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 optargs)
  #:use-module (system base compile)
  #:use-module (system vm loader)
  #:use-module (tsukundere game support)
  #:replace (language-for-file resolve-module* resolve-interface*))

(define language-table
  '((".scm" . scheme)
    (".el" . elisp)
    (".clj" . lokke)
    (".cljc" . lokke)
    (".w" . wisp)
    (".wisp" . wisp)
    ("" . scheme)))

(define (more-recent? stat1 stat2)
  ;; Return #t when STAT1 has an mtime greater than that of STAT2.
  (or (> (stat:mtime stat1) (stat:mtime stat2))
      (and (= (stat:mtime stat1) (stat:mtime stat2))
           (>= (stat:mtimensec stat1)
               (stat:mtimensec stat2)))))

(define (language-for-file file)
  (cdr (find (compose (cute string-suffix? <> file) car)
             language-table)))

(define* (resolve-module* name #:optional (autoload #t) (version #f) #:key (ensure #t))
  (cond
   ((resolve-module name #f version #:ensure #f)
    => identity)
   (autoload
    (and-let* ((file-base (string-join (map symbol->string name) "/"))
               (file (search-path %load-path
                                  file-base
                                  (map car language-table)
                                  #t))
               (compiled (compiled-file-name file))
               (language (language-for-file file)))
      (support-language! language)
      (let ((precompiled (search-path %load-compiled-path file-base
                                      %load-compiled-extensions)))
        (cond
         ((and precompiled (more-recent? (stat precompiled) (stat file)))
          (load-thunk-from-file precompiled))
         ((and (file-exists? compiled)
               (more-recent? (stat compiled) (stat file)))
          (load-thunk-from-file compiled))
         (else
          (if %load-should-auto-compile
              (begin
                (%warn-auto-compilation-enabled)
                (format (current-warning-port) ";;; compiling ~a\n" file)
                (let ((compiled (compile-file file #:from language)))
                  (format (current-warning-port) ";;; compiled ~a\n" compiled)
                  (save-module-excursion (load-thunk-from-file compiled))))
              (let ((bytecode
                     (call-with-input-file file
                       ;; XXX: Tune optimization level
                       (cute read-and-compile <> #:from language))))
                (save-module-excursion (load-thunk-from-memory bytecode))))))))
    (resolve-module name autoload version #:ensure ensure))
   (else
    (and ensure (resolve-module name #f version #:ensure ensure)))))

(define* (resolve-interface* name #:key version #:rest args)
  (resolve-module* name #t version #:ensure #f)
  (apply resolve-interface name args))
