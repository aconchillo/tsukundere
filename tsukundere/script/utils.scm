;; Copyright © 2020, 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere script utils)
  #:use-module (tsukundere agenda)
  #:use-module (tsukundere components textures)
  #:use-module (tsukundere components text)
  #:use-module (tsukundere entities)
  #:use-module (tsukundere entities menu)
  #:use-module (tsukundere entities plain)
  #:use-module (tsukundere history)
  #:use-module (tsukundere math)
  #:use-module (tsukundere game)
  #:use-module (tsukundere game internals)
  #:use-module (tsukundere i18n)
  #:use-module (tsukundere sound)
  #:use-module (tsukundere script)
  #:autoload   (tsukundere script transitions) (transition->procedure)
  #:use-module (sdl2 render)
  #:use-module (sdl2 surface)
  #:use-module (sdl2 video)
  #:use-module (ice-9 arrays)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (tween
            add! clear! move!
            current-window-width current-window-height
            current-window-center background
            say make-speaker
            make-person-type
            roll-credits
            input menu
            script-lambda* (script-lambda* . skripto*) skripto)
  #:re-export ((play-sound . sound)
               (play-music . music)
               script
               history
               call/paused)
  #:re-export-and-replace (pause sleep))

;; Call @var{proc} every @var{step} milliseconds for a total of @var{duration},
;; with values interpolated between @var{start} and @var{end} according to
;; @var{ease} and @var{interpolation}.
;; When @var{restore?} is truthy (the default), guard against glitches by
;; calling @code{(proc start)} when the tween is aborted.
(define* (tween duration start end proc #:key
                (step 1)
                (ease smoothstep)
                (interpolate lerp)
                (restore? #t))
  (call-with-restore
   (let* ((script (current-script))
          (t0 (script-time script)))
     (lambda ()
       (let loop ((t t0)
                  (t* (+ t0 duration)))
         (if (>= t t*)
             (proc end)
             (let ((alpha (ease (/ (- t t0) (- t* t0)))))
               (proc (interpolate start end alpha))
               (sleep step)
               (loop (script-time script) t*))))))
   (lambda () (when restore? (proc start)))))

(define add! (cut scene-add! (current-scene) <...>))
(define clear! (cut scene-clear! (current-scene) <>))
(define (move! entity pos)
  (set! (position entity) pos))

(define (current-window-width)  (array-ref (%bottom-right) 0))
(define (current-window-height) (array-ref (%bottom-right) 1))
(define (current-window-center) (array-copy (%center)))

;; Set @var{surface-or-texture} as the current background, positioning it
;; in @var{position} and anchoring it in @var{anchor}.
;; Use @var{transition} to transist between backgrounds.  @xref{transitions}.
(define* (background surface-or-texture #:key
                     (position (current-window-center))
                     (anchor 'center)
                     transition
                     (duration 1000))
  (let ((%background (entity-at (current-scene) 'background))
        (entity ((if (surface? surface-or-texture)
                     surface->entity texture->entity)
                 surface-or-texture
                 #:position position #:anchor anchor)))
    ((transition->procedure transition) duration %background (list entity) #t)))

;; Let @var{speaker} say @var{text}.  If @var{append?} is true, use
;; @code{text-add!} to add the text to the end of what is currently displayed
;; rather than resetting it.
(define* (say speaker text #:optional append? #:key markup?)
  (let ((speaker (G_ speaker))
        (text (G_ text)))
    (reset-text! (entity-at (current-scene) 'text 'speaker) speaker #t)
    (if append?
        (text-add! (entity-at (current-scene) 'text 'text) text
                   #:markup? markup?)
        (reset-text! (entity-at (current-scene) 'text 'text) text
                     #:markup? markup?))))

;; Make a procedure, that calls @code{say} with the rest of its arguments
;; binding @var{speaker} to @var{name}.
(define (make-speaker name)
  (cute say name <> <...>))

;; Roll the credits in @var{scene}, which span a total vertical size of
;; @var{length} pixels, for @var{duration} milliseconds.
;; While the credits are rolling, vertically scroll them from the bottom of the
;; current window to the top before resetting their position.
(define (roll-credits scene length duration)
  (let* ((credits-layer (layer-at scene 'credits))
         (pos0 (map position credits-layer)))
    (call/paused
     (lambda ()
       (for-each move! credits-layer (map array-copy pos0))
       (tween duration (current-window-height) (- length)
              (lambda (y)
                (for-each
                 (lambda (obj p)
                   (array-map! (position obj)
                               + p (s32vector 0 (round y))))
                 credits-layer pos0))
              #:ease identity)
       (for-each move! credits-layer pos0))
     #:scene scene
     #:key-press (const *unspecified*)
     #:key-release (const *unspecified*)
     #:mouse-press (const *unspecified*)
     #:mouse-release (const *unspecified*)
     #:text-input (const *unspecified*)
     #:hard-pause? #t)))

(define* (%input speaker prompt)
  (let ((input (make-fluid ""))
        (%running (make-fluid #t)))
    (call/paused
     (lambda ()
       (while (fluid-ref %running)
         (reset-text! (entity-at (current-scene) 'text 'speaker) speaker #t)
         (reset-text! (entity-at (current-scene) 'text 'text)
                      (string-join (list prompt (fluid-ref input)) "\n")
                      #t)
         (sleep 1)))
     #:hard-pause? #t
     #:key-press
     (lambda (key code mod repeat?)
       (case key
         ((return) (fluid-set! %running #f))
         ((backspace)
          (when (fluid-ref %running)
            (let ((i (fluid-ref input)))
              (fluid-set! input
                          (substring i 0
                                     (max 0 (1- (string-length i))))))))))
     #:key-release (const *unspecified*)
     #:mouse-press (const *unspecified*)
     #:mouse-release (const *unspecified*)
     #:text-input
     (lambda (txt)
       (when (fluid-ref %running)
         (fluid-set! input
                     (string-append (fluid-ref input) txt)))))
    (fluid-ref input)))

(define input
  (case-lambda
    ((prompt) (%input "" prompt))
    ((speaker prompt) (%input speaker prompt))))

(define-syntax menu
  (syntax-rules ()
    ((_ var (options options* ...))
     (let ((val
            (run-menu (make-menu-layer
                       (quote (options options* ...))
                       #:base (entity-at (current-scene) 'menu)))))
       (history var val)))))

(define-syntax script-lambda*
  (syntax-rules ()
    ((_ args exp exp* ...)
     (lambda* args
       (script exp exp* ...)))))

(define-syntax skripto
  (syntax-rules ()
    ((_ (name . args) exp exp* ...)
     (define name (script-lambda* args exp exp* ...)))))
