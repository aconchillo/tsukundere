;; Copyright © 2020, 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere sound)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 match)
  #:use-module (ice-9 q)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-26)
  #:use-module (tsukundere agenda)
  #:use-module ((tsukundere game internals)
                #:select (current-script *game-agenda*))
  #:use-module (tsukundere history)
  #:use-module (tsukundere preferences)
  #:use-module (tsukundere script)
  #:use-module (tsukundere utils)
  #:use-module ((sdl2 mixer) #:prefix mixer:)
  #:re-export ((mixer:load-chunk . load-sound)
               (mixer:load-music . load-music)
               (mixer:stop-music! . stop-music!))
  #:export (init-sound!
            load-sound load-sounds play-sound
            load-music load-music* play-music
            sound-volume music-volume volume))

(define mixer:MAX_VOLUME 128)

(define (volume? volume)
  (and (number? volume) (<= 0 volume 1)))

(define *sound-volume* (make-preference 'sound-volume 1/2
                                        #:validate?
                                        volume?))

(define *music-volume* (make-preference 'music-volume 1/2
                                        #:validate?
                                        volume?))

(add-preference-hook! *sound-volume*
                      (lambda (val)
                        (mixer:set-channel-volume! #f (factor->sdl-volume val))))

(add-preference-hook! *music-volume*
                      (lambda _ (%adjust-music-volume)))

(define sound-volume (preference->procedure *sound-volume*))
(define music-volume (preference->procedure *music-volume*))

(define (factor->sdl-volume factor)
  (round (* mixer:MAX_VOLUME (min 1 (max 0 factor)))))

(define (sdl-volume->factor volume)
  (/ volume mixer:MAX_VOLUME))

(define %music-volume (make-object-property))

(define volume
  (make-procedure-with-setter
   (match-lambda*
     (((? mixer:chunk? chunk))
      (sdl-volume->factor (mixer:set-chunk-volume! chunk -1)))
     (((? mixer:music? music))
      (or (%music-volume music) 1)))
   (match-lambda*
     (((? mixer:chunk? chunk) (? volume? volume))
      (mixer:set-chunk-volume! chunk (factor->sdl-volume volume)))
     (((? mixer:music? music) (? volume? volume))
      (set! (%music-volume music) volume)))))

(define *now-playing* (make-undefined-variable))
(define (%adjust-music-volume)
  (and-let* (((variable-bound? *now-playing*))
             (track (variable-ref *now-playing*)))
   (mixer:set-music-volume!
     (factor->sdl-volume
      (* (volume track) (music-volume))))))

(define* (init-sound! #:key
                      (mixer-formats '(flac mp3 ogg))
                      (mixer-frequency 44100)
                      (mixer-chunk-size 1024))
  (mixer:mixer-init mixer-formats)
  (mixer:open-audio #:frequency mixer-frequency #:chunk-size mixer-chunk-size)
  (mixer:set-channel-volume! #f (factor->sdl-volume (sound-volume))))

(define *sound-queue* (make-q))

;; @deffnx {(tsukundere script utils) alias} sound
;; Play sound file @var{sound} in @var{channel} looping it @var{loops} times.
;; If @var{volume} is given, first set the volume of the sound file to it.
(define* (play-sound sound #:key (loops 0) channel)
  (enq! *sound-queue* (list 'sound sound #:loops loops #:channel channel))
  (on-next-script-line
   (set-car! *sound-queue*
             (filter (match-lambda (('sound . args) #f) (_ #t))
                     (car *sound-queue*)))
   (sync-q! *sound-queue*)))

(define (do-play-sound dt)
  (for-each
   (match-lambda
     (('sound . args) (apply mixer:play-chunk! args))
     (('music (or #nil #f) _) (mixer:stop-music!))
     (('music music loops)
      (mixer:set-music-volume!
       (factor->sdl-volume (* (volume music) (music-volume))))
      (mixer:play-music! music loops)))
   (car *sound-queue*))
  (set-car! *sound-queue* '())
  (set-cdr! *sound-queue* #f))
(schedule-plan! *game-agenda* do-play-sound)

;; @deffnx {(tsukundere script utils) alias} music
;; Play music file @var{music} in @var{channel} looping it @var{loops} times.
;; If @var{volume} is given, first set the music volume to it.
;; If @var{music} is @code{#f}, stop playing instead.
(define* (play-music music #:key loops)
  (enq! *sound-queue* (list 'music music loops)))
